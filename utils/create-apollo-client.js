import ApolloClient from 'apollo-boost';
import fetch from 'node-fetch';

const GetApolloClient = new ApolloClient({
    uri: '/api/ico',
    fetch
});

export default GetApolloClient;
