// eslint-disable-next-line import/prefer-default-export
export const FETCH_ICOS = 'FETCH_ICOS';
export const SET_CURRENCY = 'SET_CURRENCY';

export interface fetchIcosAction {
    type: typeof FETCH_ICOS;
    payload: any;
}

export interface setCurrencyAction {
    type: typeof SET_CURRENCY;
    payload: string;
}

export type IcosActionTypes = fetchIcosAction | setCurrencyAction;
export type AppActions = IcosActionTypes;
