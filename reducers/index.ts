import { FETCH_ICOS, SET_CURRENCY } from '../types/actions';

const initialState = {
    isLoading: false,
    icos: [],
    error: [],
    currency: ''
};

type actionType = {
    type: string;
    payload: any;
};

const actionsMap = {
    [`${FETCH_ICOS}_PENDING`]: state => {
        return { ...state, isLoading: true };
    },
    [`${FETCH_ICOS}_FULFILLED`]: (state, action) => {
        return {
            ...state,
            isLoading: false,
            icos: action.payload.icos
        };
    },
    [`${FETCH_ICOS}_REJECTED`]: (state, action) => {
        return { ...state, isLoading: false, error: action.payload.error };
    },
    [SET_CURRENCY]: (state, action) => {
        return { ...state, currency: action.payload };
    }
};

const reducer = (state = initialState, action: actionType) => {
    const reducerFunction = actionsMap[action.type];
    return reducerFunction ? reducerFunction(state, action) : state;
};

export default reducer;
