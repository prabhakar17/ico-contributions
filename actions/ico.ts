import { FETCH_ICOS, SET_CURRENCY, AppActions } from '../types/actions';
import GetApolloClient from '../utils/create-apollo-client';
import ICOS_QUERY from '../queries';

const client = GetApolloClient;

const fetchIcosAction = (): AppActions => ({
    type: FETCH_ICOS,
    payload: async () => {
        const result = await client.query({
            query: ICOS_QUERY
        });
        const icoContributions = result.data.IcoContributions;
        return { icos: icoContributions };
    }
});

export const setCurrencyAction = (currency): AppActions => ({
    type: SET_CURRENCY,
    payload: currency
});

export default fetchIcosAction;
