import { SET_CURRENCY, AppActions } from '../types/actions';

const setCurrencyFilterAction = (): AppActions => ({
    type: SET_CURRENCY,
    payload: ''
});

export default setCurrencyFilterAction;
