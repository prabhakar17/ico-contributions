import React from 'react';
import { Provider } from 'react-redux';
import Home from '../components/home';
import store from '../store';

const Layout = () => {
    return (
        <Provider store={store}>
            <Home />
        </Provider>
    );
};

export default Layout;
