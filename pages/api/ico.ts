import { ApolloServer } from 'apollo-server-micro';
import ICO_DATA from '../../mock/data';
import typeDefs from '../../schema/ico';

const resolvers = {
    Query: {
        IcoContributions() {
            return ICO_DATA;
        }
    }
};

const apolloServer = new ApolloServer({ typeDefs, resolvers });

export const config = {
    api: {
        bodyParser: false
    }
};

export default apolloServer.createHandler({ path: '/api/ico' });
