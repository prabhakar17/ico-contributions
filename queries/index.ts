import { gql } from 'apollo-boost';

const ICOS_QUERY = gql`
    {
        IcoContributions {
            address
            currency
            value
            txid
        }
    }
`;

export default ICOS_QUERY;
