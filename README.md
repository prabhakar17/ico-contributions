# Ico Contributions

![](ico.gif)

To run the app locally

* npm install
* npm run dev
* Go to http://localhost:3000

## Tech stack
* NodeJs
* NextJs for server side rendering
* React
* Graphql
* Apollo Client
* Redux
* Typescript