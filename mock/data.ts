const ICO_DATA = [
    {
        address: '113npncHphBgk1SKZ2wairsVrdScWmKKA3',
        currency: 'BTC',
        value: '0665213',
        txid: 'r9y87l07y82ox7585xy06zp3c755f54478szb062yq17s06dddopvs98d45u0170'
    },
    {
        address: '14ZAc82UV9BTC2yGFxtyQATGobrc2D5Vsq',
        currency: 'BTC',
        value: '2313309',
        txid: 'p6a61z39m23kw1470ci95va6x006y51242gab172jt07w57gyaobgk13v00b3415'
    },
    {
        address: '1Kszdv8HKeqmro9ASLTCEFGmbSX4qs1Jy6',
        currency: 'BTC',
        value: '8520800',
        txid: 'n1j85i74c20xl8162ps62yq9v987u62144qnw225lw86j09nkmbxob03n32l8818'
    },
    {
        address: '1AycKK8NrVtbwYkoYjRNm8Cbp3QkydjRg',
        currency: 'ETH',
        value: '7198798',
        txid: 'k9a36e57t39ur4492xg66ki9s887o18643prq511fy94a40ymygfsy71r03i0866'
    },
    {
        address: '1D2YkY6YeX3AzHP9PNAQ4RhLcew3zrXyaZ',
        currency: 'BTC',
        value: '7099466',
        txid: 'z8e60p18e25mk6305db70js2z193d11961bhu237kx89m84rowywpt29l28z7775'
    },
    {
        address: '123ffR7APLubiqVpEZD3TLLXupyPu2Bvvb',
        currency: 'ETH',
        value: '8773310',
        txid: 'i8g21i98a55rg3316rp03yi7w976n04275awf729sp46k61rmjlmns54b37j6361'
    },
    {
        address: '1Ai2t3B5r9jFJSvkwUQcSZcvpW8g2Jehxe',
        currency: 'BTC',
        value: '3004006',
        txid: 'f3y54p95d60lg7394yp77ld6i509z24708nzk566nq69u21msvuwat56o71w5942'
    },
    {
        address: '197nHjXnvh12ixeiC6a4dnxMZfT3MTVbSP',
        currency: 'BTC',
        value: '3472620',
        txid: 'd5o01i86f92jz1707og09xp6t052z76992xbc379kp00h83jlombdf16i64j5799'
    },
    {
        address: '1CWrnGZ6uRKNeUUmkYyFuUbwFtT9Fkjwpb',
        currency: 'BTC',
        value: '7894608',
        txid: 'c8j46c68x74qy7911lq27jc2x666v13990xvo948bs10j00hemnnnc81z42l6662'
    },
    {
        address: '1BZimJrQLdmgJG8gTHEny97L9Hb9nn4wkv',
        currency: 'ETH',
        value: '0972588',
        txid: 'o4t42j33r28uj3859ge21wj8j791t66636gwi521hv77c14iuohtnu95i03z9875'
    },
    {
        address: '1K4gvqsx8yYuZBATSHFoBtqs6h9GFEuktt',
        currency: 'LTC',
        value: '2500072',
        txid: 'g3e24u62v11yo1929ol20xr4b786q33184jvt270pp79i54npqtdpq89y07i6330'
    },
    {
        address: '1LxQRA3dbnbDququTn3SoRXP5hTnm9c9XE',
        currency: 'LTC',
        value: '4095396',
        txid: 's1p77u49c85zn3678fj48rg5f307n39016bvl641cb15d89msdgpaz61b89j3545'
    },
    {
        address: '136LrqG9kFG9BPF5s2sTLEdYoraqZ3Uz5N',
        currency: 'LTC',
        value: '9553479',
        txid: 'k8n18o96c65jb7646ik30jq5c871k61588jcr337sy87p73nxypfdx47t10a1083'
    },
    {
        address: '14GNqzjZe1CbK8G2QSchwre6LD8Tu2CKfe',
        currency: 'ETH',
        value: '7074302',
        txid: 'g7n82q22h20gd3876rh43qd3u191d98695fcm572jx60y63qnoryyc40d04l6792'
    },
    {
        address: '1GwtavEQgWEmBJ6yqTdsKLgSGKLAZFVQ5k',
        currency: 'ETH',
        value: '7415283',
        txid: 'c1v59n50c28dl7449ox64yu5a497q21996wyb747ng98p04gpqqhax66e59i2455'
    },
    {
        address: '19FuUETHPefjhwxwbWj7D7gu2Dn4akeGaq',
        currency: 'ETH',
        value: '0004295',
        txid: 'e9y81f89x16si3213wk35bi6d272b33571vgg410ve01q61whcxjti90l29u3615'
    },
    {
        address: '15MBSxfc1EMe3WJpa27oGeLGvHkvb4FZXQ',
        currency: 'LTC',
        value: '8511627',
        txid: 's5i13i36s51gq7692ur11vl0l821v72125fba523cg27f61myfrtss12f55c1057'
    },
    {
        address: '1H4eUVuyf37JCWLULCwNL3XhLTCL82hidD',
        currency: 'LTC',
        value: '2612050',
        txid: 'b9x35s84z00lc5209jd48hy3l756b33315aya408bd16k81jcgsuch46p01x8063'
    },
    {
        address: '1HUqqic9SowUApFAtntrcUXF1gDZSbZvsk',
        currency: 'BTC',
        value: '4676967',
        txid: 'p4d83g31y00os6532oo67zk3t360r13153duc777eo39s68qabeota03v76o4711'
    },
    {
        address: '1FXPwkcLe4Eak45jDs5ZqiBebxAzjdkc8o',
        currency: 'LTC',
        value: '2309823',
        txid: 'h1p24s73z17yw1064yw53db6c287e97542qgt059ie08c07fsixqvo92o64v0755'
    },
    {
        address: '189JN42JCBRPL3FNh1PRjh2kaGJH5WmiyY',
        currency: 'BTC',
        value: '5477163',
        txid: 'c7m29h72w16rb2813lu58cs4p152m82868mlm764jb94e17asipupz12r06g1268'
    },
    {
        address: '1BjtEZaFXt6Rmkk87JUp66YCCZhQM71yxQ',
        currency: 'BTC',
        value: '5127490',
        txid: 'w3q60u20z52uf8506fa05qf0k396s58715wef499af94a02exnlwgr02g35t3190'
    },
    {
        address: '176BAjDaXUn3ddbsdwsMhsTBWSzgC4LWZh',
        currency: 'BTC',
        value: '1427809',
        txid: 'k9x15h30g06wk9021yk62xu1f679j55562ias674pd42p08rliqhkj85m32i7369'
    },
    {
        address: '1CKrUwdt9ibQB9peqHhQbJzsKqJtS2jsJK',
        currency: 'BTC',
        value: '1818714',
        txid: 'k9d72d68o99qe0045vr53db5x912d70877aue821hq87c01cwmowgp83w30o4410'
    },
    {
        address: '1F1QvKzgegFGs5aJmtBh6fBRSHhovEpkfQ',
        currency: 'ETH',
        value: '0491730',
        txid: 'd6u15z35a42oq2059cd13zf5h787c53855lsl622ns91m88nroynha78t30f3683'
    },
    {
        address: '1JuoFxZdnKSEcRhYq5JaTcRVFvLA7jvp69',
        currency: 'BTC',
        value: '4112723',
        txid: 'o5g54j00v75sk2877to57yu7x239t71057kwu085mv72w39cmhizju55k09a5593'
    },
    {
        address: '1KsFZUhgSGB9FWA9iEsVsdNm31hmzs8voE',
        currency: 'BTC',
        value: '0307225',
        txid: 'h7w59o43w49ec6217wm51iz9v707i37442inv448uy22y13wwhxpud36s95k5680'
    },
    {
        address: '1J1Q7jzLfxyAD1RXX9hCdo7vc1LnKeARZr',
        currency: 'BTC',
        value: '6137094',
        txid: 'f8m84s86f86bt1269bk33jt7p248k12503zis798km99w07zytevzu44a39o2904'
    },
    {
        address: '12wv2G6yk5NzfEewC3j2jJ2xaXBE8Go29s',
        currency: 'LTC',
        value: '9424531',
        txid: 'i3t64s47w24by8412ck19vs0o820a05383wlg803dy87g11lplaadk41r08m7840'
    },
    {
        address: '1EvEkVeZkQVNBFbBsdg7h2xcGmcvS8vYFj',
        currency: 'BTC',
        value: '0252853',
        txid: 'v7u46r37l07rx6107tk48ke8a163e31912hcd479gn58d42qqqnyqj96i22l4339'
    },
    {
        address: '1JfGMQXnGDUMFUCrsA1jfj1HqNbQxfisnx',
        currency: 'BTC',
        value: '8070585',
        txid: 'w9e91i11k53ja2260xw42np7l376r12588lku994lz53t53fmruqrv46b94g4017'
    },
    {
        address: '1J4d92quGAKHZZfwvqum7ikP5Z7qTDWy7W',
        currency: 'BTC',
        value: '7179304',
        txid: 'a4h55o29t25mk8037hh33yj3w452v73260czg075rl23t33ssmejkb89k38n7342'
    },
    {
        address: '1HcykCmXuE1YyEy5CJ6Zj4MeKz54iwf6b8',
        currency: 'BTC',
        value: '2689540',
        txid: 'c4s80e90p16zq4332kb42hy4h169f97661noa901rn77v29zavtduv94t60e4959'
    },
    {
        address: '1AWqU6keK5bNfo6y8cvPscTZVgTL2Nsqzw',
        currency: 'BTC',
        value: '9761552',
        txid: 'y5a50l97k38jw7087lm75ca0i827a19892kfy324pe91v22bbjffsv62v79f2505'
    },
    {
        address: '13QZm4KobSU2pk9tCphZQzVC9RFBNHZTjb',
        currency: 'LTC',
        value: '1915769',
        txid: 'k0l71q04f76nt8301ui96xz7e876d59324xhn375zn39f35mkgefjy20n92o3445'
    },
    {
        address: '1B2Y8rkwTYZq8HmYUrsVZ8PyX1B3xxEpE1',
        currency: 'ETH',
        value: '3760273',
        txid: 'f0a09l37p58qg4802vu49iq7w519q60247tkv932rl74y29cirefdm61e43m2857'
    },
    {
        address: '12R5een6jPN1w52FMENMjHWK6UA3gkwC8p',
        currency: 'BTC',
        value: '9519443',
        txid: 'w2m02r51u58yr9953ke26rz4d198l59872khr614au17e15rbetrga74m75u8250'
    },
    {
        address: '1NR9wnXkpxdzDwSVk7c74Ckm5xHGaRGErb',
        currency: 'BTC',
        value: '9509889',
        txid: 'r5o15f30e11oz3022pa21kd5v113v51935wdt953hl50v30uhiwqgb71r83r1268'
    },
    {
        address: '13DBJtTKH9p7CfzNTMSkpQKU9xGRBXefuW',
        currency: 'LTC',
        value: '4724993',
        txid: 'c0j90m51w57mf0258jz49jn1l883u00758rsy000my47j22gmzfdug63y54i4792'
    },
    {
        address: '1BZan8xfmNcibdWnGJiGJZsMo5Ezeysbis',
        currency: 'BTC',
        value: '5988240',
        txid: 'i5e59s69a91uo2028ue05za5d011x05645moq370wu34w50tbctqbi42l82w0857'
    },
    {
        address: '14yhYVWGQ91D1W9chXXYYRSPtdKETHQvYK',
        currency: 'BTC',
        value: '8011553',
        txid: 'v7a39f69t99yh0653th54ux9a987e16781dtg815uw75x13upmfqlg12l54w5881'
    },
    {
        address: '1A3zWj9uhZHvidXr4auWojn49bJHjeCLUZ',
        currency: 'LTC',
        value: '6447257',
        txid: 'f2y41a88x69lx3022yy68xx2n793c25224qdj087jb49t30acirdwj29z44a0778'
    },
    {
        address: '1CpwadNTnk87PVdKFfqMP96xzHLnVMe5bD',
        currency: 'ETH',
        value: '0257858',
        txid: 't8f20n22b27tt5663dd49fk7c401u83043wzh754rh49r75hxnvrao63i43v8785'
    },
    {
        address: '18JHLaabrhW1hPT2bo2x6Pfu3odbR2g8s4',
        currency: 'BTC',
        value: '9880958',
        txid: 'n9s19n11t60wj1345oa95ah9i756k45118thg970bk75t04qdygqod79x68e2049'
    },
    {
        address: '1ADkCzX1uAfJAkqz2J8ESEas2VLWcnAGHH',
        currency: 'ETH',
        value: '2936654',
        txid: 'c3t81i52r37yy8941ln57el4c070r68212frp127vs50p49dqskfrb42l30j5625'
    },
    {
        address: '1DVL8TnasZgFd4FZfXEA3uRMqawMyNgb1t',
        currency: 'ETH',
        value: '9147946',
        txid: 'v6h81c33y58zr5907jy62wg9r555k46869wyo793sw69l21lnnmixd72a10x6096'
    },
    {
        address: '1MRdycbxz3mhCPCz1SoyiUUkdJAVJMK4Nx',
        currency: 'BTC',
        value: '1129313',
        txid: 'q9q76t03d52th7834qy87sp0t743j78566kfo416kc57g48azfnxvc51b34n0468'
    },
    {
        address: '1KsKhtCv6u6DnmVbj9KkQXgakbzeXijvWk',
        currency: 'LTC',
        value: '3810928',
        txid: 'a6r30y14n53yu5363im91ge8a270y36266efs849bx90s86keahzuj91r00u7198'
    },
    {
        address: '1G7Kogj6dwVtW6mxJ6Dhbx1LaxP6xyYSPv',
        currency: 'LTC',
        value: '9761659',
        txid: 'r5v31s51c09qq9525um53vz1y564a59524zxy981cb69u81vihtdvs60j49s5247'
    },
    {
        address: '1KxUW1eZw9UHfvygrvHHBLK5rEsm5G4oaG',
        currency: 'ETH',
        value: '8168930',
        txid: 'l2y65p86e34ss6398vm67xa2e822u37722vxa369ix39e50inckpxq94d37b9873'
    },
    {
        address: '18BBxfuWb3NeLW3RzCgCvMA7nWGYpf3V5r',
        currency: 'BTC',
        value: '5005430',
        txid: 'b1o26e40m07zr6436lc00tj8h501y86045lym192px39m80bagraig90d15n6422'
    },
    {
        address: '1MBx85Ue4uJnW9SW6Uvu783JpaXrkeFF1J',
        currency: 'BTC',
        value: '8503851',
        txid: 'd7c48e36v13hp7553eb69nz2w988p85726fpb609fz40m46beknugr05w05n4463'
    },
    {
        address: '16nqqHRNqBvD3UgHmtXfmEUAsCNM8NEUw9',
        currency: 'LTC',
        value: '7656750',
        txid: 'j9o78k56i33sy4135dt52jl4b393h56832rwk514dp81l24xjfsudj54d47b1860'
    },
    {
        address: '1G1BJYHR6HXzxfi9CAc6sK74q5NCts5zWy',
        currency: 'BTC',
        value: '3292547',
        txid: 'm6f75u60l68iy0946yu33yq2n406c39532esa743rg15z91ktguffv57e13i7582'
    },
    {
        address: '12fLnCxxQajp7uqLSGgG1jsShcva93iBmj',
        currency: 'LTC',
        value: '5768456',
        txid: 's9e66p22p53lz8349tg78dp3w757a71060psf649kd03h32wwgyxwk54r38k5373'
    },
    {
        address: '148cyajyRVMn9wXTaYszarp1uh2bBaQCbT',
        currency: 'BTC',
        value: '8449029',
        txid: 'a8l73v37s25wy4636id86hk4t927q40059laj814px61r66vfqjwkc01n32p1672'
    },
    {
        address: '1CMfkZzRrchkC5x4ZR7y1sWrtPX6d3imsF',
        currency: 'BTC',
        value: '4080262',
        txid: 'p8k75u02v69mn3232oz10be4x915p81184fxy612cy55i39mnusunn67z34u6711'
    },
    {
        address: '1CEtZkHR23JT1yN1Vkt536U7wCGPnF3ZQR',
        currency: 'BTC',
        value: '8244226',
        txid: 'w4p87g48j17jj7619au76kw1j806j72739boz404oj61y14uhgbwrq14k65d4214'
    },
    {
        address: '1CBETHZJ9waEYYSqg9Z537q1ZePPNCcBLM',
        currency: 'LTC',
        value: '4989924',
        txid: 'b8f20e30o14bw7046lk66vq3k427a07489yrn425ix17h41vfvwnkq27j22h8227'
    },
    {
        address: '1KmyuVijkh5HvCLzPNUaPc4VrHDFB8iGNp',
        currency: 'BTC',
        value: '8530213',
        txid: 'm1e99j04n88an7468wm55vn9k501k95909nnt893og80x22onqcpqb35s95m1239'
    },
    {
        address: '1Ed4mHRzENbtDowxw7ta1kqhXHf7fv7DiJ',
        currency: 'ETH',
        value: '9863934',
        txid: 'r0y14q88t99vk2845ay62wt0l621e83855vdw683ns70x74mjqktaq97s92a2703'
    },
    {
        address: '1CG5Ms8MBtVFEnsEF6sfk1jL6MZ9QkY368',
        currency: 'LTC',
        value: '2318913',
        txid: 'x5c12n24z06dv5813fc83ok1o883y16893jxt455rh89n67sqaotti08l19i9411'
    },
    {
        address: '1CrxeD4SaBnJ4WHCQq8VE65BZRdeVw7V2N',
        currency: 'BTC',
        value: '6879444',
        txid: 'i0a23n34n26hu3125nz48wn9r620a08424sfs572ic39p55kssyhkn77r87c3343'
    },
    {
        address: '199GUPvhg39hqLfRE3ry2K3siRRSdYHhDH',
        currency: 'LTC',
        value: '5455935',
        txid: 'n8c83q26t19rp2664ob47qz6y862y18373kqg578qw11t63sovwzwf03n88b4087'
    },
    {
        address: '1E9Hmh4V1mXBherf79NrnyP6vDCCs12FZX',
        currency: 'BTC',
        value: '7414654',
        txid: 'l9y30o27u08ek2413dr47ab1s400f25550iro387ud90g57aiupkxq88f39o9453'
    },
    {
        address: '1AbU3dwo44tVWooMtDNdEXAFjvzARCAxSw',
        currency: 'BTC',
        value: '0963359',
        txid: 'j6o60g83l01eb2795mx13jb4m662j11783viw273rz41f41eszsvlg95c11c7338'
    },
    {
        address: '1HgCf1vMS6oWUMkmNwELFq43qCDVYQvmdC',
        currency: 'BTC',
        value: '4065966',
        txid: 's0w87n37j28mc1676nt67op4o275q06902ypx349so01v37rbouvxm14y89f0456'
    },
    {
        address: '1LYhfyzjz38n44Y6YfZwU4CxFzss6bb9Cg',
        currency: 'BTC',
        value: '4176165',
        txid: 'j9g85n39q28qt6349si62ee5f089j55193jew052uk95m02sykvihe29o51u9009'
    },
    {
        address: '18m833PMpb85t779hXjgEM7uAogipSNA3U',
        currency: 'ETH',
        value: '4561938',
        txid: 'z6s34j01d93iy9090zn60dk0n189i32003tef081mq90i71vgwgatx15x91q8285'
    },
    {
        address: '1DTvUeFM3pHMJm9xDp5d6bTxTvLWxowpfQ',
        currency: 'ETH',
        value: '2873336',
        txid: 'i6i15i52b35nz0664ip09jn4x123k71303gxe656em80x89exlxbyd97i04y3869'
    },
    {
        address: '147MMtnKKDwFNfzmnfyxnyTReGYC5BjQK7',
        currency: 'ETH',
        value: '3096261',
        txid: 'l9m77b55i39tf3251wi37du9r781k49298ajp302ss18f75ochnknj25a62e4647'
    },
    {
        address: '1KeXRUA8fXSXNbqwmctdBuAMrxTtqi7XSb',
        currency: 'ETH',
        value: '2207126',
        txid: 'p5b09t24h53ui9518fs15my1x854w43516eev565ff27k30yzvmjba79z12r4684'
    },
    {
        address: '1EvdkporcohnAVZHFYTrcv5fMDpecdGVQF',
        currency: 'LTC',
        value: '8000792',
        txid: 'y8n27g49h29ww1230df63fq4h810f59364bfw950cz15s62tyldntx68q30t3236'
    },
    {
        address: '1aSvhLWA7X7pmfgFBuh2pCJCEtm9MtHHo',
        currency: 'LTC',
        value: '6560694',
        txid: 'n5c31h11n14fh9168iw71mi6y341v96811axp486gm91p68cjdkpeb07u54z9526'
    },
    {
        address: '1BabEsCdppdwbh4NbrExsVDa1yxuud32uQ',
        currency: 'BTC',
        value: '3877312',
        txid: 'q0t24q25f53vp1122qt05ra4e451d72782uck557xw79q98pmvgklo57q18l1562'
    },
    {
        address: '1NiuWMCjMYTP2aTXZWP7K7TLFLXyXbkqcd',
        currency: 'BTC',
        value: '6524382',
        txid: 'k1n46g36w96li6503td20dn8i076g52376mmg566wf65m02radrilw15o96u4624'
    },
    {
        address: '1JvxV9hJPsjbNA7dKmzce2rKJ7ut27hPQJ',
        currency: 'ETH',
        value: '7279578',
        txid: 'm6t58i49i92ts9366pq91vb7z388b39719duc214wf71z18uknfnfd96z33l0816'
    },
    {
        address: '1HQLAedJLkF7R2wZye5RYyPrskAjFoQY66',
        currency: 'BTC',
        value: '1733570',
        txid: 'm5w99w03e03af1548fd93yo4z181v95179iob628si83r50jfxrmhh58y84g6308'
    },
    {
        address: '1HJbwWCy66L7gwFoxxJmHXJ7F5F7koQts1',
        currency: 'LTC',
        value: '2166235',
        txid: 'y2p74m78g74nv6691zb64ov7a245a36327tnx970ad56z03ljshxzp40v45d6905'
    },
    {
        address: '19NA5smaUN2GuvoSab4HtY5xETekvrqtyS',
        currency: 'ETH',
        value: '0120188',
        txid: 'x9q76j77z02yr8509dh47so2v398r09545tew672id06m82jwmcovy94y34m4512'
    },
    {
        address: '19VS84xEh37CgLSvGvmzxLfwKwrmccQhSd',
        currency: 'BTC',
        value: '1275179',
        txid: 'c1p81f16m85hh2960ju91jo7b498t30989dqy908gq14s35hmbujra95f43d3451'
    },
    {
        address: '19SABbAEgRBCJC2zJsh7vjWTxMjg36Nxz4',
        currency: 'LTC',
        value: '4356395',
        txid: 'y3n92n33o91kf0753sw07vb9j211b40209qhi771xk75s14hptzluc69q69a0855'
    },
    {
        address: '1CZojZkFxvdZrhzG7shBhmGcGiSQJcKpBP',
        currency: 'BTC',
        value: '7361265',
        txid: 'h6d07b75n50up3988tg95es0q010f20707igp357lb36x82jdaqbja25t40k7081'
    },
    {
        address: '1PHzjJ2p6ftK7667WAyJMd9jdoKY9PjqM2',
        currency: 'LTC',
        value: '4740175',
        txid: 'k3p37r93e69zr3266ok65bk6e995e71635mms437jz41f96hsvshqd74t31x7648'
    },
    {
        address: '1CqwKy8mfhTdT2whuSNyTPqCyf2aJEvtdn',
        currency: 'LTC',
        value: '3262806',
        txid: 'g7b57w89i00ou0673pm49cv3d068l29128hkj770ae70c56hgmgbwk85q34y3897'
    },
    {
        address: '1Q7oTwdu8CV8hHxUwRUAusPyz2LPKVpUD1',
        currency: 'BTC',
        value: '1028489',
        txid: 'o2j73f39f86ad2000vs87nw2o681b13195jub064df54p01qnrrmwt03o48v3714'
    },
    {
        address: '1QEdzse2JtHtHxT2s4UPDMyzLp8UkBiKUt',
        currency: 'LTC',
        value: '5711578',
        txid: 'e6m27d78n99nl3753nh78rf4l985t97844ozo595js34s15esmsbod60q84z9495'
    },
    {
        address: '1NCMF7Qb1qMoTFpyufudDp52SnYjf9ZiKQ',
        currency: 'LTC',
        value: '3521189',
        txid: 'z7i31b26l02ku5999au75il3t081q88327tya127zj29i15tlqelnj48r62u6890'
    },
    {
        address: '1P6VMtfkAnJDqXhFWWQA6eEeT2kLSBYpk2',
        currency: 'BTC',
        value: '3887251',
        txid: 'z7t20o10k16im3389xj02xd0s352o92165nbl683qm43l33aLTCrdo72w36n0897'
    },
    {
        address: '1MCJ8zvWnWGFvUmrTo9a784ceFm7tcjGaT',
        currency: 'BTC',
        value: '4923126',
        txid: 'l4t44c56h44yo2484jl91lw6c067f66352wdd406bv24j66zvrnoiw16t04l9373'
    },
    {
        address: '15HpquhxXf4JbuJnkkuNL1FkdxBXimSp3d',
        currency: 'LTC',
        value: '4681586',
        txid: 'm9z77t32v93nv0321ah26hd7l117p06916ser570di39t94ctiakgg51j38v4138'
    },
    {
        address: '1MJqftwc84vtrsCxRXbFfzANeMhjUjwr9Q',
        currency: 'ETH',
        value: '0561606',
        txid: 'c9s06d94c78do3931rj21fo0k145d84232jqn517us49p54nhjkilr89l76f8951'
    },
    {
        address: '1413NqYknb1wRHcj7M2gCDKVmfN5Kij9vD',
        currency: 'ETH',
        value: '3271511',
        txid: 'q9p83k23t00vf9987sw02wn7t753h28931rjh671ze64q13pdzeqgk19v39f4025'
    },
    {
        address: '1BfbkXz8Hcn8uPW41zBN5dy9aYTRhDRwqk',
        currency: 'ETH',
        value: '4532982',
        txid: 's5g93v86d77iz9185nj80uw1a312a28167oze282nf85k49lzdytxj39m86g6903'
    },
    {
        address: '1P915TRHq1PR5Pp4B9mBYNNcYkVcehQseM',
        currency: 'BTC',
        value: '7785838',
        txid: 'a1l49p52c38mt7425os17nm8y069e58404uxx262bo33r93qhbdibe17n11a5683'
    },
    {
        address: '1FUv9eDwWebdyZAsucoZoViA3N56CrvqqR',
        currency: 'BTC',
        value: '8482712',
        txid: 'h5l72h11p70rn6701oo99mk1q321h66338vak571iq08m12offqspv83k73s7685'
    },
    {
        address: '1E7GsFsWrUFL2yJZinXF9QovhDYNoEbHUz',
        currency: 'BTC',
        value: '0735415',
        txid: 's3r42x55s45om2724lq68ko7b087p33308tjw745yg42b49lokxlpe68n57i3026'
    },
    {
        address: '18VwEZFurnYnrEMyvtRgH9TiqJYFBQ9cV4',
        currency: 'BTC',
        value: '4076334',
        txid: 'c5r36j12h84ez1336dn20db6p870g61818ogl336be71l68wvhvrrv02x87b9207'
    },
    {
        address: '1HAeBjC3n4dMdzseC22Z6MgLRGUoLDnoP',
        currency: 'LTC',
        value: '8234469',
        txid: 'b9j68f13m14dp4163fs90ao4q626k19854nvs841uf49e05rkvfbyl85o66y7147'
    },
    {
        address: '1ptz2d59AZH46EWqQizPQ1CLcqoLrcVEd',
        currency: 'ETH',
        value: '7250061',
        txid: 'l2p15g77x79oc6469lo36cy8a168x83791ift514mr81v61vkkikcj89w53g7927'
    }
];

export default ICO_DATA;
