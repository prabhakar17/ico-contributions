import { createStore, applyMiddleware } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';
import promise from 'redux-promise-middleware';
import logger from 'redux-logger';
import rootReducer from '../reducers';
import { AppActions } from '../types/actions';

export type AppState = ReturnType<typeof rootReducer>;

const store = createStore(
    rootReducer,
    applyMiddleware(promise, thunk as ThunkMiddleware<AppState, AppActions>, logger)
);

export default store;
