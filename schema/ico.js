import { gql } from 'apollo-boost';

const typeDefs = gql`
    type ico {
        address: String!
        currency: String!
        value: String!
        txid: String!
    }

    type Query {
        IcoContributions: [ico]
    }
`;

export default typeDefs;
