import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import Ico from './ico';

import fetchIcosAction from '../actions/ico';

const Home = ({ fetchIcos, icos, isLoading, error }) => {
    React.useEffect(() => {
        fetchIcos();
    }, [fetchIcos]);

    if (isLoading) return <div>Loading...</div>;
    // eslint-disable-next-line no-console
    if (error) console.error('Error fetching data: ', error);
    if (!icos) return <div>No data to load.</div>;

    return icos.length > 0 && <Ico icos={icos} />;
};

const filterByCurrency = (icos, currency) =>
    currency ? icos?.filter(ico => ico.currency === currency) : icos;

const mapStateToProps = (state: any) => {
    return {
        icos: filterByCurrency(state.icos, state.currency),
        isLoading: state.isLoading,
        error: state.error
    };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        fetchIcos: () => dispatch(fetchIcosAction())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
