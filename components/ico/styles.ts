import { createStyles } from '@material-ui/core';

const styles = () =>
    createStyles({
        container: {
            backgroundImage:
                'radial-gradient(#D7D7D7 1px, transparent 1px), radial-gradient(#d7d7d7 1px, transparent 1px)',
            maxWidth: '98%',
            margin: 'auto',
            backgroundPosition: '0 0, 25px 25px',
            backgroundSize: '50px 50px'
        }
    });

export default styles;
