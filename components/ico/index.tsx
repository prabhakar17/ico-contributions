import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import styles from './styles';
import IcoContributionsTable from '../table';
import IcoBar from '../chart/ico-bar';
import CurrencySelector from '../filter/currency';

const Ico = ({ classes, icos }) => {
    return (
        <Grid container spacing={3} className={classes.container} justify="center">
            <Grid item xs={12} sm={2}>
                <CurrencySelector icos={icos} />
            </Grid>
            <Grid item xs={12} sm={6}>
                <IcoBar icoContributions={icos} />
            </Grid>
            <Grid item />
            <Grid item xs={12} sm={8}>
                <IcoContributionsTable icoContributions={icos} />
            </Grid>
            <Grid item />
        </Grid>
    );
};

export default withStyles(styles)(Ico);
