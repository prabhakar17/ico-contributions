import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/styles/withStyles';
import styles from './styles';

const IcoContributionsTable = ({ classes, icoContributions }) => {
    const rows = icoContributions?.sort((a, b) => {
        const currencyA = a.currency?.toUpperCase();
        const currencyB = b.currency?.toUpperCase();

        return currencyA >= currencyB ? 1 : -1;
    });

    if (!icoContributions) return null;

    return (
        <TableContainer component={Paper} className={classes.icoTableContainer}>
            <Table stickyHeader aria-label="ico contributions table">
                <TableHead>
                    <TableRow>
                        <TableCell>Currency</TableCell>
                        <TableCell>Value</TableCell>
                        <TableCell>Address</TableCell>
                        <TableCell>Transaction Id</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map(row => (
                        <TableRow key={row.address}>
                            <TableCell>{row.currency}</TableCell>
                            <TableCell>{row.value}</TableCell>
                            <TableCell component="th" scope="row">
                                {row.address}
                            </TableCell>
                            <TableCell>{row.txid}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default withStyles(styles)(IcoContributionsTable);
