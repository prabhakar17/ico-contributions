import { createStyles } from '@material-ui/core';

const styles = () =>
    createStyles({
        icoTableContainer: {
            maxHeight: 'calc(100vh - 200px)'
        }
    });

export default styles;
