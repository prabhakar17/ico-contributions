import createStyles from '@material-ui/core/styles/createStyles';

const styles = ({ breakpoints }) =>
    createStyles({
        formContainer: {
            display: 'flex',
            alignItems: 'baseline'
        },
        formControl: {
            marginRight: 32,
            minWidth: 125,
            [breakpoints.down('xs')]: {
                marginRight: 16
            }
        }
    });

export default styles;
