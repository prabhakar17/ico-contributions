import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import withStyles from '@material-ui/styles/withStyles';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Button } from '@material-ui/core';
import styles from './styles';
import { setCurrencyAction } from '../../actions/ico';

const CurrencySelector = ({ classes, icos, setCurrency, currency }) => {
    const currencySet = new Set(icos.map(ico => ico.currency));
    const currencyList = Array.from(currencySet);

    const handleChange = event => {
        setCurrency(event.target.value);
    };

    const handleReset = () => setCurrency('');

    return (
        <div className={classes.formContainer}>
            <FormControl className={classes.formControl}>
                <InputLabel id="currency-label">Currency</InputLabel>
                <Select
                    labelId="currency-label"
                    id="currency-select"
                    value={currency}
                    onChange={handleChange}
                >
                    {currencyList?.map(currencyItem => (
                        <MenuItem value={`${currencyItem}`}>{currencyItem}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            {currency && (
                <Button variant="outlined" color="primary" onClick={handleReset}>
                    Reset
                </Button>
            )}
        </div>
    );
};

const mapStateToProps = (state: any) => {
    const { icos, currency } = state;
    return { icos, currency };
};

const mapDispatchToProps = (dispatch: any) => {
    return {
        setCurrency: currency => dispatch(setCurrencyAction(currency))
    };
};

export default compose(
    withStyles(styles),
    connect(mapStateToProps, mapDispatchToProps)
)(CurrencySelector);
