import createStyles from '@material-ui/core/styles/createStyles';

const styles = () =>
    createStyles({
        icoContainer: {
            width: 400,
            height: 200
        }
    });

export default styles;
