import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

const options: Highcharts.Options = {
    chart: {
        type: 'bar',
        height: 150
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: ['']
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Total value (M)'
        }
    },
    legend: {
        align: 'right',
        verticalAlign: 'top',
        layout: 'vertical'
    },
    plotOptions: {
        series: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: [
        {
            name: 'BTC',
            data: [5],
            type: 'bar'
        },
        {
            name: 'ETH',
            data: [2],
            type: 'bar'
        },
        {
            name: 'LTC',
            data: [3],
            type: 'bar'
        }
    ]
};

const groupAndSum = (array, key) => {
    let totalValue = 0;
    return array.reduce((r, o) => {
        if (!r[o[key]]) {
            // eslint-disable-next-line no-param-reassign
            r[o[key]] = o.value;
        } else {
            totalValue = parseFloat(r[o[key]]) || 0;
            totalValue += parseFloat(o.value);
            // eslint-disable-next-line no-param-reassign
            r[o[key]] = totalValue;
        }

        return r;
    }, {});
};

const numberToMoney = amount => {
    if (amount >= 1.0e9) return Math.ceil(amount / 1.0e9);
    if (amount >= 1.0e6) return Math.ceil(amount / 1.0e6);
    if (amount >= 1.0e3) Math.ceil(amount / 1.0e3);
    return amount;
};

const IcoBar = ({ icoContributions }) => {
    const totalValueByCurreny = icoContributions ? groupAndSum(icoContributions, 'currency') : [];

    const seriesData = [];
    const entries = Object.entries(totalValueByCurreny);
    entries.forEach(ele =>
        seriesData.push({
            name: ele[0],
            data: [numberToMoney(ele[1])],
            type: 'bar'
        })
    );

    const optionsWithSeriesData = { ...options, series: seriesData };

    return <HighchartsReact highcharts={Highcharts} options={optionsWithSeriesData} />;
};

export default IcoBar;
