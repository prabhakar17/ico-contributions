FROM node:12-alpine
WORKDIR /usr/src/app
COPY . .
RUN npm install
EXPOSE 6796
ENV NODE_ENV=production
RUN npm run build
CMD [ "npm", "start" ]